export const relevanceScoreFilter = [
  {
    id: 1,
    value: "high",
    label: "High",
  },
  {
    id: 2,
    value: "medium",
    label: "Medium",
  },
  {
    id: 3,
    value: "low",
    label: "Low",
  },
];

export const keySkillsFilter = [
  "HTML",
  "Java",
  "JavaScript",
  "React",
  "HTML_2",
  "Java_2",
  "JavaScript_2",
  "React_2",
];

export const locations = [
  { id: 1, checked: false, label: "Delhi" },
  { id: 2, checked: false, label: "Bangalore" },
  { id: 3, checked: false, label: "Mumbai" },
  { id: 4, checked: false, label: "Chennai" },
  { id: 5, checked: false, label: "Hyderabad" },
];

export const users = [
  {
    id: 1,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Google Analytics"],
    relevanceMatch: "Medium",
  },
  {
    id: 2,
    work: "7y 10m",
    expectedSalary: "7 lacs",
    preferredLocation: ["Delhi", "Mumbai", "Hyderabad"],
    keySkills: ["Java", "Java2"],
    relevanceMatch: "High",
  },
  {
    id: 3,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Google Analytics"],
    relevanceMatch: "Medium",
  },
  {
    id: 4,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Chennai"],
    keySkills: ["React", "HTML", "CSS"],
    relevanceMatch: "Medium",
  },
  {
    id: 5,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai", "Hyderabad"],
    keySkills: ["React", "HTML"],
    relevanceMatch: "High",
  },
  {
    id: 6,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Google Analytics", "HTML"],
    relevanceMatch: "Low",
  },
  {
    id: 7,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Java2"],
    relevanceMatch: "Low",
  },
  {
    id: 8,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Google Analytics"],
    relevanceMatch: "Medium",
  },
  {
    id: 9,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai", "Chennai"],
    keySkills: ["Java", "Google Analytics"],
    relevanceMatch: "Low",
  },
  {
    id: 10,
    work: "6y 10m",
    expectedSalary: "6 lacs",
    preferredLocation: ["Delhi", "Mumbai"],
    keySkills: ["Java", "Google Analytics"],
    relevanceMatch: "Medium",
  },
];
