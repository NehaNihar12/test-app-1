import * as actionTypes from "./filterTypes";
export const getRelevanceMatch = (value) => {
  return {
    type: actionTypes.SET_SELECTED_CATEGORY,
    payloads: { selectedCategory: value },
  };
};
export const getKeySkills = (keySkills) => {
  return {
    type: actionTypes.SET_SKILLS,
    payloads: { keySkills: keySkills },
  };
};
export const getLocations = (location) => {
  //console.log(location);
  return {
    type: actionTypes.SET_CHECKED,
    payloads: { location: location },
  };
};
export const setRefineSearch = (boolValue) => {
  //console.log(location);
  return {
    type: actionTypes.SET_REFINE_SEARCH,
    payloads: { doRefineSearch: boolValue },
  };
};
export const setApplyFilters = (boolValue) => {
  return {
    type: actionTypes.APPLY_FILTERS,
    payloads: { applyFilters: boolValue },
  };
};
