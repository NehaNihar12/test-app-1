import * as actionTypes from "./listTypes";
export const filterUsers = (selectedCategory, keySkills, locations) => {
  return {
    type: actionTypes.FILTER_USERS,
    payloads: {
      selectedCategory: selectedCategory,
      keySkills: keySkills,
      locations: locations,
    },
  };
};
