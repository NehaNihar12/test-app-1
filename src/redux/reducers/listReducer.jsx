import { users } from "../../constants";
import * as actionTypes from "../list/listTypes";
// const applyFilters = () => {
//     let updatedList = dataList;

//     // Rating Filter
//     if (selectedRating) {
//       updatedList = updatedList.filter(
//         (item) => parseInt(item.rating) === parseInt(selectedRating)
//       );
//     }

const initialState = {
  updatedUsers: users,
};
export default function listReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FILTER_USERS: {
      const selectedCategory = action.payloads.selectedCategory;
      const keySkills = action.payloads.keySkills;
      const locations = action.payloads.locations;
      let updatedUsers = users;

      //   if (
      //     selectedCategory === null &&
      //     keySkills.length === 0 &&
      //     locations.length === 0
      //   ) {
      //     updatedUsers = users;
      //   }
      //relevance match filter
      if (selectedCategory) {
        updatedUsers = updatedUsers.filter(
          (user) =>
            user.relevanceMatch.toLowerCase() === selectedCategory.toLowerCase()
        );
      }
      //key skills filter
      if (keySkills.length !== 0) {
        updatedUsers = updatedUsers.filter((user) =>
          user.keySkills.some((skill) => keySkills.includes(skill))
        );
      }
      //locations filter
      if (locations.length !== 0) {
        updatedUsers = updatedUsers.filter((user) =>
          user.preferredLocation.some((location) =>
            locations.includes(location)
          )
        );
      }
      return {
        ...state,
        updatedUsers: [...updatedUsers],
      };
    }
    default:
      return state;
  }
}
