import { combineReducers } from "redux";
import filterReducer from "./filterReducer";
import listReducer from "./listReducer";

export const rootReducer = combineReducers({
  // Define a top-level state field named `todos`, handled by `todosReducer`
  list: listReducer,
  filters: filterReducer,
});
