import * as actionTypes from "../filters/filterTypes";

const initialState = {
  selectedCategory: null,
  keySkills: [],
  locations: [],
  doRefineSearch: false,
  applyFilters: false,
};

export default function filterReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_SELECTED_CATEGORY: {
      let value = action.payloads.selectedCategory;

      return {
        ...state,
        selectedCategory: !value ? null : value,
      };
    }
    case actionTypes.SET_SKILLS:
      return {
        ...state,
        keySkills: [...action.payloads.keySkills],
      };
    case actionTypes.SET_CHECKED: {
      let newLoc = [];
      const index = state.locations.indexOf(action.payloads.location);
      if (index === -1) {
        newLoc = [...state.locations, action.payloads.location];
      } else {
        newLoc = state.locations.filter(
          (element) => element !== action.payloads.location
        );
      }

      return {
        ...state,
        locations: newLoc,
      };
    }
    case actionTypes.SET_REFINE_SEARCH:
      return {
        ...state,
        doRefineSearch: action.payloads.doRefineSearch,
      };
    case actionTypes.APPLY_FILTERS:
      return {
        ...state,
        applyFilters: action.payloads.applyFilters,
      };
    default:
      return state;
  }
}
