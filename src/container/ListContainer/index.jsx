import { useEffect } from "react";
import { connect } from "react-redux";
import CardComp from "../../components/CardComp";
import "./styles.css";
import { users } from "../../constants";
import { filterUsers } from "../../redux/list/listActions";

const ListContainer = (props) => {
  useEffect(() => {
    if (props.applyFilters)
      props.filterUsers(
        props.selectedCategory,
        props.keySkills,
        props.locations
      );
    // else
    //   props.filterUsers(
    //     props.selectedCategory,
    //     props.keySkills,
    //     props.locations
    //   );
  }, [props.applyFilters]);
  const updatedUsers =
    props.selectedCategory === null &&
    props.keySkills.length === 0 &&
    props.locations.length === 0
      ? users
      : props.updatedUsers;
  return (
    <div className="main">
      <header>List</header>
      {updatedUsers.map((user) => (
        <CardComp data={user}></CardComp>
      ))}
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    applyFilters: state.filters.applyFilters,
    selectedCategory: state.filters.selectedCategory,
    keySkills: state.filters.keySkills,
    locations: state.filters.locations,
    updatedUsers: state.list.updatedUsers,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    filterUsers: (selectedCategory, keySkills, locations) =>
      dispatch(filterUsers(selectedCategory, keySkills, locations)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
