import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
  getRelevanceMatch,
  getKeySkills,
  getLocations,
  setRefineSearch,
  setApplyFilters,
} from "../../redux/filters/filterActions";

import CheckboxComp from "../../components/CheckboxComp";
import FilterListToggle from "../../components/FilterListToggle";
import SearchBar from "../../components/SearchBar";
import {
  keySkillsFilter,
  relevanceScoreFilter,
  locations,
} from "../../constants";
import "./styles.css";

const FiltersContainer = (props) => {
  useEffect(() => {
    let boolVal =
      props.selectedCategory !== null ||
      props.keySkills.length !== 0 ||
      props.locations.length !== 0
        ? true
        : false;
    props.setRefineSearch(boolVal);
    props.setApplyFilters(false);
  }, [props.selectedCategory, props.keySkills, props.locations]);

  const handleSelectCategory = (event, value) => {
    props.getRelevanceMatch(value);
  };

  const handleSetSkill = (event, value) => {
    props.getKeySkills(value);
  };

  const handleChecked = (e) => {
    props.getLocations(e.target.value);
  };

  const handleRefineSearch = () => {
    props.setRefineSearch(false);
    props.setApplyFilters(true);
  };

  return (
    <div className="main flex-column">
      <header>Filters</header>
      {/* Relevance score filter */}
      <div className="filter-container ">
        <FilterListToggle
          options={relevanceScoreFilter}
          value={props.selectedCategory}
          selectToggle={handleSelectCategory}
        ></FilterListToggle>
      </div>

      {/* key skills filter*/}
      <div className="filter-container ">
        <SearchBar
          skills={keySkillsFilter}
          selectedSkills={props.keySkills}
          setSkill={handleSetSkill}
        />
      </div>

      {/* location filter*/}
      <div className="filter-container ">
        <CheckboxComp
          locations={locations}
          checked={props.locations}
          handleChecked={handleChecked}
        />
      </div>
      {props.doRefineSearch && (
        <button data-btn="refine-search-btn" onClick={handleRefineSearch}>
          Refine
        </button>
      )}
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    selectedCategory: state.filters.selectedCategory,
    keySkills: state.filters.keySkills,
    locations: state.filters.locations,
    doRefineSearch: state.filters.doRefineSearch,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRelevanceMatch: (value) => dispatch(getRelevanceMatch(value)),
    getKeySkills: (value) => dispatch(getKeySkills(value)),
    getLocations: (value) => {
      //console.log(value);
      return dispatch(getLocations(value));
    },
    setRefineSearch: (boolVal) => dispatch(setRefineSearch(boolVal)),
    setApplyFilters: (boolVal) => dispatch(setApplyFilters(boolVal)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FiltersContainer);
