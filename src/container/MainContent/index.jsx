import React from "react";
import FiltersContainer from "../FiltersContainer";
import ListContainer from "../ListContainer";
import "./styles.css";

const MainContent = (props) => {
  return (
    <div className="main-content">
      <section className="filters-container-wrap">
        <FiltersContainer></FiltersContainer>
      </section>
      <section className="list-container-wrap">
        <ListContainer></ListContainer>
      </section>
      {props.children}
    </div>
  );
};

export default MainContent;
