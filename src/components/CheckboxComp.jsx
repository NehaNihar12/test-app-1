import {
  Card,
  CardContent,
  FormControlLabel,
  Checkbox,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Stack,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const CheckboxComp = ({ locations, checked, handleChecked }) => {
  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        Locations
      </AccordionSummary>

      <AccordionDetails>
        <Stack spacing={2} width="90%">
          <Card>
            {locations.map(({ id, label }) => {
              return (
                <CardContent key={id}>
                  <FormControlLabel
                    label={label}
                    control={
                      <Checkbox
                        color="primary"
                        value={label}
                        checked={checked.includes(label)}
                        onChange={handleChecked}
                      />
                    }
                  />
                </CardContent>
              );
            })}
          </Card>
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
};
export default CheckboxComp;
