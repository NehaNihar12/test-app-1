import { Card, CardContent } from "@mui/material";

const CardComp = ({ data }) => {
  return (
    <Card className="card">
      <CardContent key={data.id}>
        <div className="flex-col">
          <div className="job-tuple-main flex-row">
            <div className="job-description-tuple">
              <div className="flex-row" style={{ display: "flex" }}>
                <span className="job-des-title">work</span>
                <span>{data.work}</span>
              </div>
              <div className="flex-row" style={{ display: "flex" }}>
                <span className="job-des-title">expectedSalary</span>
                <span>{data.expectedSalary}</span>
              </div>
              <div className="flex-row" style={{ display: "flex" }}>
                <span className="job-des-title">preferred location</span>
                <span>{data.preferredLocation.toString()}</span>
              </div>
              <div className="flex-row" style={{ display: "flex" }}>
                <span className="job-des-title">key Skills</span>
                <span
                  className="key-skills flex-row"
                  style={{ display: "flex" }}
                >
                  {data.keySkills.map((skill) => (
                    <div>{skill}</div>
                  ))}
                </span>
              </div>
            </div>
            <div className="job-tuple-profile"></div>
            <div className="job-tuple-social-icons"></div>
          </div>
          <div className="job-tuple-footer">
            <div>{data.relevanceMatch}</div>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default CardComp;
