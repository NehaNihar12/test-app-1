import React from "react";
import {
  ToggleButtonGroup,
  ToggleButton,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { v4 as uuidv4 } from "uuid";
const FilterListToggle = ({ options, value, selectToggle }) => {
  //style for toggle button
  const toggle = {
    fontFamily: `'Raleway', sans-serif`,
    fontSize: ".8rem",
    border: "1px solid rgba(0, 0, 0, 0.12)",
    // borderRadius: "10px",
    "&.MuiToggleButtonGroup-groupedHorizontal:not(:last-child)": {
      borderRadius: "2px",
    },
    "&.MuiToggleButtonGroup-groupedHorizontal:not(:first-child)": {
      borderRadius: "2px",
      border: "1px solid rgba(0, 0, 0, 0.12)",
    },
    "&.Mui-selected": {
      borderRadius: "2px",
      background: "lightBlue",
      color: "#fff",
    },
    "&.MuiToggleButton-root": {
      "&:hover": {
        background: "lightBlue",
        color: "#fff",
      },
    },
  };
  const root = {
    width: "100%",
    justifyContent: "space-between",
    "&.MuiToggleButtonGroup-root": {
      padding: "8px",
    },
  };
  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        Relevancy Match
      </AccordionSummary>

      <AccordionDetails>
        <ToggleButtonGroup
          id={uuidv4()}
          value={value}
          exclusive
          onChange={selectToggle}
          size="small"
          sx={root}
        >
          {options.map(({ label, id, value }) => (
            <ToggleButton sx={toggle} key={id} value={value}>
              {label}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
      </AccordionDetails>
    </Accordion>
  );
};

export default FilterListToggle;
