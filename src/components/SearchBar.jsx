import {
  Stack,
  Autocomplete,
  TextField,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
const SearchBar = ({ skills, selectedSkills, setSkill }) => {
  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        Key Skills
      </AccordionSummary>

      <AccordionDetails>
        <Stack spacing={2} width="250px">
          <p>{selectedSkills.length} filter(s) selected</p>
          <Autocomplete
            multiple
            limitTags={1}
            id="multiple-limit-tags"
            options={skills}
            //getOptionLabel={(skills) => skills}
            defaultValue={[skills[1]]}
            renderInput={(params) => (
              <TextField {...params} label="Key Skills" placeholder="skills" />
            )}
            value={selectedSkills}
            onChange={setSkill}
            sx={{ width: "90%", outline: "none" }}
          />
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
};

export default SearchBar;
