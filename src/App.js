import "./App.css";

import MainContent from "./container/MainContent";
import Navbar from "./container/Navbar";
import Topbar from "./container/Topbar";
function App() {
  return (
    <div className="App">
      <Topbar></Topbar>
      <Navbar></Navbar>
      <MainContent></MainContent>
    </div>
  );
}

export default App;
